# Buession Spring Cloud

基于 Spring Cloud 2 的扩展。

[![Build Status](https://travis-ci.org/buession/buession-springcloud.svg?branch=master)](https://travis-ci.org/buession/buession-springcloud)
[![Coverage Status](https://img.shields.io/codecov/c/github/buession/buession-springcloud/master.svg)](https://codecov.io/github/buession/buession-springcloud?branch=master&view=all#sort=coverage&dir=asc)
[![Maven Central](https://img.shields.io/maven-central/v/com.buession.springcloud/buession-springcloud-common.svg)](https://search.maven.org/search?q=g:com.buession.springcloud)
[![GitHub release](https://img.shields.io/github/release/buession/buession-springcloud.svg)](https://github.com/buession/buession-springcloud/releases)
[![License](https://img.shields.io/badge/license-Apache%202-4EB1BA.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

## Requirements

- JDK 1.8+

## Introduction

- git clone https://github.com/buession/buession-springcloud
- cd buession-springcloud/buession-springcloud-parent && mvn install

## Maven

- https://search.maven.org/search?q=g:com.buession.springcloud
- https://mvnrepository.com/search?q=com.buession.springcloud

## Documentation
---

- 中文 https://github.com/buession/buession-springcloud/wiki/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98
- English https://github.com/buession/buession-springcloud/wiki/FAQ

## License

The Buession Framework is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).